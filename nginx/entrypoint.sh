#!/bin/sh

# Var checks
if [ -n "$BACKEND_HOST" ] && [ -n "$BACKEND_PORT" ]; then
    # Inject variables
    sed -i s/__BACKEND_HOST__/$BACKEND_HOST/g /etc/nginx/conf.d/keycloak.conf
    sed -i s/__BACKEND_PORT__/$BACKEND_PORT/g /etc/nginx/conf.d/keycloak.conf

    if [ ! -f /etc/nginx/ssl/cert.pem ]; then
        echo "Generating self-signed SSL"

        mkdir -p /etc/nginx/ssl-self-signed

        openssl req \
            -x509 \
            -sha512 \
            -newkey rsa:4096 \
            -keyout /etc/nginx/ssl-self-signed/key.pem \
            -nodes \
            -out /etc/nginx/ssl-self-signed/cert.pem \
            -days 3650 \
            -subj "/C=US/ST=DeuxSevre/L=Niort/O=docker/OU=keycloak/CN=keycloak-server"

        cp /etc/nginx/ssl-self-signed/* /etc/nginx/ssl
    fi

    # Start nginx
    nginx -g "daemon off;"
else
    echo "ERROR: please provide BACKEND_HOST, BACKEND_PORT"
fi